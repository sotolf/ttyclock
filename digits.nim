type 
  Digit* = range[0..9]
  Typeface* = array[5,string]

func `$`*(self: Typeface): string =
  for i, line in self:
    result &= line
    if i < self.high:
      result &= "\n"

func toTypeface*(dgt: Digit): Typeface =
  case dgt
  of 1 : ["    ██", "    ██","    ██","    ██","    ██"]
  of 2 : ["██████", "    ██","██████","██    ","██████"]
  of 3 : ["██████", "    ██","██████","    ██","██████"]
  of 4 : ["██  ██", "██  ██","██████","    ██","    ██"]
  of 5 : ["██████", "██    ","██████","    ██","██████"]
  of 6 : ["██████", "██    ","██████","██  ██","██████"]
  of 7 : ["██████", "    ██","    ██","    ██","    ██"]
  of 8 : ["██████", "██  ██","██████","██  ██","██████"]
  of 9 : ["██████", "██  ██","██████","    ██","    ██"]
  of 0 : ["██████", "██  ██","██  ██","██  ██","██████"]

const colonDivider: Typeface = ["  ", "██", "  ", "██", "  "]
const digits = "1234567890"

func concat*(digits: openarray[Typeface]): Typeface =
  for i in 0..4:
    for dcount, digit in digits:
      result[i] &= digit[i]
      if dcount < digits.high:
        result[i] &= "  "

func add*(a: var Typeface, b: Typeface) =
  for i in 0..4:
    a[i] &= "  "
    a[i] &= b[i]

func add*(a: var Typeface, d: Digit) =
  a.add d.toTypeface

func addDivider*(a: var Typeface) =
  a.add colonDivider

func toTypeface*(s: string): Typeface =
  for ch in s:
    if ch in digits:
      result.add (ch.ord - 48).toTypeface
    if ch == ':':
      result.addDivider

func width*(tf: Typeface): int =
  tf[0].len

func height*(tf: Typeface): int =
  tf.len
