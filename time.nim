import std/[times, strformat, os, lenientops]
import digits
import illwill

type ClockMode = enum
  Standard, Decimal

# Setup illwill stuff
proc exitProc() {.noconv.} =
  illwillDeinit()
  showCursor()
  quit 0

illwillInit fullscreen=true
setControlCHook exitProc
hideCursor()

var tb = newTerminalBuffer(terminalWidth(), terminalHeight())

proc drawTypeface(tb: var TerminalBuffer, clock: Typeface) =
  # draw the clock in the middle of the window
  let y = 2 #(tb.height div 2) - (clock.height div 2) - 1
  let x = 0 #(tb.width div 2) - (clock.width div 2) - 1

  tb.clear()
  tb.setForegroundColor fgGreen
  for i, line in clock:
    tb.write x, (y + i), line

  tb.resetAttributes()

proc drawStandardTime(tb: var TerminalBuffer) =
  let dt = now()
  let clock = dt.format("HH:mm").toTypeface
  tb.drawTypeface clock

const secondsInDecHour = 8_640
const secondsInDecMinute = 86.4

proc drawDecimalTime(tb: var TerminalBuffer) =
  let tm = now()
  let hours = tm.hour()
  let minutes = tm.minute()
  let seconds = (hours * 3600) + (minutes * 60)
  let decHours = seconds div secondsInDecHour
  let decMinutes = ((seconds mod secondsInDecHour) / secondsinDecMinute).int
  let clock = (&"{decHours:0>2}:{decMinutes:0>2}").toTypeface
  tb.drawTypeface clock

func nextMode(cur: var ClockMode) =
  try:
    cur = cur.succ
  except RangeDefect, OverflowDefect:
    cur = ClockMode.low

func prevMode(cur: var ClockMode) =
  try:
    cur = cur.pred
  except RangeDefect, OverflowDefect:
    cur = ClockMode.high

# Start illwill event-loop
var mode: ClockMode = Standard
while true:
  var key = getKey()
  case key
  of Key.Escape, Key.Q: exitProc()
  of Key.Left: mode.prevMode
  of Key.Right: mode.nextMode
  else: discard

  case mode
  of Standard: tb.drawStandardTime()
  of Decimal: tb.drawDecimalTime()

  tb.display()
  sleep(20)


